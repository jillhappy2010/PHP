<?php include './util/common.php'; ?>
<?php include './pages/index/function.php'; ?>
<?php
    setSession();
 ?>


<!DOCTYPE html>
<html lang="en">

<?php include './pages/component/pblHeader.php'; ?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include './pages/component/admin_nav.php'; ?>

        <!-- /#page-wrapper -->
        <?php include './pages/index/wrapper.php'; ?>

    </div>
    <!-- /#wrapper -->

    <?php include './pages/component/pblFooter.php'; ?>
</body>

</html>
