<?php include './util/common.php'; ?>
<?php include './pages/posts/function.php'; ?>
<?php ob_start(); ?>

<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=uid2utr6rknmi1z8vervm2axids5rgs8bfe3zoypeeg93zli"></script>

<!DOCTYPE html>
<html lang="en">

<?php include './pages/component/pblHeader.php'; ?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include './pages/posts/admin_nav.php'; ?>

        <!-- /#page-wrapper -->
        <?php 

        if(isset($_GET['source'])){
        	$source=$_GET['source'];
        	include "./pages/posts/wrapper_{$source}.php";
		        
	    }else{
	    	include './pages/posts/wrapper.php';
	    };
	     ?>
    </div>
    <!-- /#wrapper -->

    <?php include './pages/component/pblFooter.php'; ?>
</body>

</html>
