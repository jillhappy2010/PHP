
        <?php
         ?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                       
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome to admin
                            <small>Juana</small>
                        </h1>
                    </div>
 
              
                </div>
                <!-- /.row -->

                    <form action="" method="post">

<div id="bulkOptionContainer" class="col-xs-4">

        <select class="form-control" name="bulk_options" id="">
        <option value="">Select Options</option>
        <option value="published">Publish</option>
        <option value="draft">Draft</option>
        <option value="delete">Delete</option>
         <option value="clone">Clone</option>
        </select>

        </div><div class="col-xs-4">

<input type="submit" name="submit" class="btn btn-success" value="Apply">
<a class="btn btn-primary" href="posts.php?source=add_post">Add New</a>

 </div><table class="table table-bordered table-hover">
  
                <thead>
                    <tr>
                <th><input id="selectAllBoxes" type="checkbox"></th>
                        <th>Id</th>
                        <th>Users</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th>Image</th>
                        <th>Tags</th>
                        <th>Comments</th>
                        <th>Date</th>
                        <th>View Post</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th>Views</th>
                    </tr>
                </thead>
                
                      <tbody>
                      

  <tr>        
                        <!-- render database start -->
                    <?php
                        renderPost();
                     ?>
                 
                            <!-- render end  -->
                    </tbody>
                    </table>

                        <script>

                            $(document).ready(function(){


                                $(".delete_link").on('click', function(){


                                    var id = $(this).attr("rel");

                                    var delete_url = "posts.php?delete="+ id +" ";


                                    $(".modal_delete_link").attr("href", delete_url);


                                    $("#myModal").modal('show');

                                });

                            });


                        </script>
 
                    </form>

            </div>
            <!-- /.container-fluid -->

        </div>
