<?php 

// query sql
	function renderPost(){
		$conn=connection();
	    $sql = "SELECT * FROM post";
	    $result = $conn->query($sql);
	    if($result->num_rows>0){
	        while($row = $result->fetch_assoc()){
	        	$rowid=$row['id'];
	        	$rowuser=$row['user'];
	        	$rowtitle=$row['title'];
	        	$rowcategory=$row['category'];
	        	$rowstatus=$row['status'];
				$rowtags=$row['tags'];
				$rowcomments=$row['comment_count'];
				$rowdate=$row['created'];
				$rowviews=$row['views_count'];
				$rowimg=$row['image'];

	            echo "
                         <td><input class='checkBoxes' type='checkbox' name='checkBoxArray[]' value='{$rowid}'></td>
                          
                        
                        <td>{$rowid} </td><td>{$rowuser}</td><td>{$rowtitle}</td><td>{$rowcategory}</td><td>{$rowstatus}</td><td><img width='100' src='../images/{$rowimg}' alt='image'></td><td>{$rowtags}</td><td><a href='post_comments.php?id={$rowid}'>{$rowcomments}</a></td><td>{$rowdate} </td><td><a class='btn btn-primary' href='../post.php?p_id={$rowid}'>View Post</a></td><td><a class='btn btn-info' href='posts.php?source=edit_post&amp;p_id={$rowid}'>Edit</a></td>

                        

                            

                         <td><form action='posts.php' method='post'><input type='hidden' name='post_id' value='{$rowid}'><input class='btn btn-danger' type='submit' name='delete' value='delete'></form></td>


                        <td><a href='posts.php?reset=135'>{$rowviews}</a></td></tr><tr>        ";
	        };
	    }; 
	     delPost();
	     $conn->close();
	};


	function createPost(){
		$conn=connection();

		if($_POST&&($_POST["create_post"])){
				$title    = $_POST["title"];
				$category = $_POST["post_category"];
				$user     = $_POST["post_user"];
				$status   = $_POST["post_status"];

				$image    = $_FILES['image']['name'];
				$image_temp = $_FILES['image']['tmp_name'];
				

				$tags     = $_POST["post_tags"];
				$content  = $_POST["post_content"];

				$created = date("Y-m-d");

				$result=move_uploaded_file($image_temp, "../images/$image");
			      echo "<h1>$image</h1>";

				$sql = "INSERT INTO post (title,category,user,status,tags,content,image,created) VALUES ('$title','$category','$user','$status','$tags','$content','$image','$created')";
				if ($conn->query($sql) === TRUE) {
					// ob_start();
				    // echo "<script>alert('added successfully')</script>";
				    $_POST["create_post"] = null;
				 //    if(!headers_sent()){
					// 	  header('Location: posts.php');
					// 	  ob_end_flush(); 
					// };
  				} else {
				    echo "Error: " . $sql . "<br>" . $conn->error;
				};
		};
	     $conn->close();		
	};


//  delete post
	function delPost(){
		global $conn;
		if($_POST&&$_POST['delete']){
			// ob_start();
			$delid = $_POST['post_id'];
			// echo "<script>alert('hello')</script>";
			// echo "<h1>{$_POST['post_id']}</h1>";
			$sql = "DELETE FROM post where id='$delid' ";

			if ($conn->query($sql) === TRUE) {
				header('Location: posts.php');
				ob_end_flush();
			};
			  // ob_end_flush();
		};

	};

	// render database data for post
    
    function renderDBPost(){
    	$p_id=$_GET['p_id'];
    	$conn=connection();
	    $sql = "SELECT * FROM post WHERE id = '$p_id' ";
	    $result = $conn->query($sql);

	    if($result->num_rows>0){
	        while($row = $result->fetch_assoc()){
				return $row;
			};

	    };
	     $conn->close();		
   };

   function renderCategory(){
    	$conn=connection();
	    $sql = "SELECT * FROM category";
	    $result = $conn->query($sql);
	    return $result;
	     $conn->close();		
   };

   function editPost(){
   			$conn=connection();
   			$p_id=$_GET['p_id'];
		if($_POST&&($_POST["edit_post"])){
				$title    = $_POST["title"];
				$category = $_POST["post_category"];
				$user     = $_POST["post_user"];
				$status   = $_POST["post_status"];

				$image    = $_FILES['image']['name'];
				$image_temp = $_FILES['image']['tmp_name'];
				

				$tags     = $_POST["post_tags"];
				$content  = $_POST["post_content"];

				$created = date("Y-m-d");

				$result=move_uploaded_file($image_temp, "../images/$image");
			      $sql = "UPDATE post ";
			      $sql .= "SET title='{$title}', author='{$user}', content='{$content}',user='{$user}',tags='{$tags}',category='{$category}' ";
			      if($image){
				      $sql .= ", image='{$image}'";
				      $sql .= "WHERE id='$p_id'"; 
				  }else{
				      $sql .= "WHERE id='$p_id'";
				  }


				if ($conn->query($sql) === TRUE) {
					header('Location: posts.php');
					ob_end_flush();
				};
	   }

   };
	
?>