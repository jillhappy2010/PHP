<?php include './util/common.php'; ?>
<?php include './pages/category/function.php'; ?>
<?php ob_start(); ?>

<!DOCTYPE html>
<html lang="en">

<?php include './pages/component/pblHeader.php'; ?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include './pages/category/admin_nav.php'; ?>

        <!-- /#page-wrapper -->
        <?php include './pages/category/wrapper.php'; ?>

    </div>
    <!-- /#wrapper -->

    <?php include './pages/component/pblFooter.php'; ?>
</body>

</html>
