
        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

                <h1 class="page-header">
                    Page Heading
                    <small>Secondary Text</small>
                </h1>

                <!-- First Blog Post -->
                <!--  beginning of loop post -->
                <?php
                    $sql = "SELECT title, author, created, image, content FROM post";
                    $result = $conn->query($sql);

                    if($result->num_rows>0){
                        while($row = mysqli_fetch_assoc($result)){
                         

                 ?>

                <h2>
                    <a href="#"><?php echo "{$row['title']}"; ?></a>
                </h2>
                <p class="lead">
                    by <a href="index.php"><?php echo "{$row['author']}"; ?></a>
                </p>
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo "{$row['created']}"; ?></p>
                <hr>
                <img class="img-responsive" <?php echo "src='./images/{$row['image']}.png'"; ?> alt="">
                <hr>
                <p><?php echo "{$row['content']}"; ?></p>
                <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

                <hr>

                <!--  end of loop post -->
                 <?
                        };
                    }else{
                        echo "<p>there is no post yet</p>";
                    };

                 ?>

                <!-- Pager -->
                <ul class="pager">
                    <li class="previous">
                        <a href="#">&larr; Older</a>
                    </li>
                    <li class="next">
                        <a href="#">Newer &rarr;</a>
                    </li>
                </ul>

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Blog Search</h4>
                    <form action="search.php" method="post">
                        <div class="input-group">
                            <input name="searchKey" type="text" class="form-control">
                            <span class="input-group-btn">
                            <button name="submit" class="btn btn-default" type="submit">
                                    <span class="glyphicon glyphicon-search"></span>
                            </button>
                            </span>
                        </div>
                    <!-- /.input-group -->
                    </form>
                </div>

                <!-- side widget category -->
                <?php include 'sidebar_category.php'; ?>
                <?php include 'sidebar_content.php'; ?>

            </div>

        </div>
        <!-- /.row -->

        <hr>