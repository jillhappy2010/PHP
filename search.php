<?php include './component/pblHeader.php'; ?>

<!DOCTYPE html>
<html lang="en">

  <?php include './component/header.php' ?>

<body>

    <?php include './component/nav.php'; ?>

    <!-- Page Content -->
    <div class="container">

        <?php include './component/search_container.php'; ?>

        <?php include './component/footer.php'; ?>

    </div>
    <!-- /.container -->

    <?php include './component/pblFooter.php'; ?>

</body>

</html>